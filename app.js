import http from 'http';
import https from 'https';
import path from 'path';
const port = 80 ;
import express from 'express';
import json from 'body-parser'
import { fileURLToPath } from 'url';
import misRutas from './router/index.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);



//inicializar la app
const app = express();

/* // Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/antonyecheagaray.com/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/antonyecheagaray.com/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/antonyecheagaray.com/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};
 */

app.set();
//Asignaciones
app.set("view engine", "ejs");

app.use(json.urlencoded({extends:true}));

//directorio global en vez de escribir manualmente la ruta
app.use(express.static(__dirname + '/public'));

app.use(misRutas.router);

/* const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

httpServer.listen(80, () => {
	console.log('HTTP Server running on port 80');
});

httpsServer.listen(443, () => {
	console.log('HTTPS Server running on port 443');
}); */ 

app.listen(port, '0.0.0.0',()=>{

    console.log("Iniciando el servidor en el puerto 3000 accede usando dominio:80 en el navegador");

}); 