import express from "express";
import json from "body-parser";
import conexion from "../models/conexion.js";
import alumnosDb from "../models/alumnos.js";
export const router = express.Router();

router.get('/', (req, res) => {
  res.render('index', { titulo: "Mi primer pagina web en EJS", nombre: "José Antonio de Jesús Osuna Echea garay" });


});

router.get('/tabla', (req, res) => {
  const params = { numero: req.query.numero, titulo: "Tabla de Multiplicar" }
  res.render('tabla', params);
});

router.post('/tabla', (req, res) => {
  const params = { numero: req.body.numero, titulo: "Tabla de Multiplicar" }
  res.render('tabla', params);
});

router.get('/cotizacion', (req, res) => {
  console.log("GET /cotizacion", req.query);
  const params = {
    folio: req.query.folio || '',
    descripcion: req.query.descripcion || '',
    precio: req.query.precio || '',
    porcentaje: req.query.porcentaje || '',
    plazos: req.query.plazos || '',
    pagoInicial: '', // Valor por defecto
    totalFin: '', // Valor por defecto
    pagoMensual: '', // Valor por defecto
    titulo: "Cotizaciones"
  }
  res.render('cotizacion', params);
});

router.post('/cotizacion', (req, res) => {
  console.log("POST /cotizacion", req.body);
  const valorAuto = parseFloat(req.body.precio) || 0;
  const pInicial = parseFloat(req.body.porcentaje) || 0;
  const plazos = parseInt(req.body.plazos) || 0;

  // Realizar cálculos
  const pagoInicial = valorAuto * (pInicial / 100);
  const totalFin = valorAuto - pagoInicial;
  const pagoMensual = totalFin / plazos;

  const params = {
    folio: req.body.folio,
    descripcion: req.body.descripcion,
    precio: req.body.precio,
    porcentaje: req.body.porcentaje,
    plazos: req.body.plazos,
    pagoInicial: pagoInicial.toFixed(2),
    totalFin: totalFin.toFixed(2),
    pagoMensual: pagoMensual.toFixed(2),
    titulo: "Cotizaciones"
  }
  res.render('cotizacion', params);
});


let rows;
router.get('/alumnos', async (req, res) => {
  rows = await alumnosDb.mostrarTodos();
  res.render('alumnos', { reg: rows, titulo: "Alumnos"});
})


let params;
router.post('/alumnos', async (req, res) => {
  try {
    if (req.body.btnBuscar) {
      // Lógica para buscar
      const matricula = req.body.matricula;
      const alumnos = await alumnosDb.consultarMatricula(matricula);
      res.render('alumnos', { reg: alumnos, titulo: "Alumnos"});
    }
    else{
    params = {
      matricula: req.body.matricula,
      nombre: req.body.nombre,
      direccion: req.body.direccion,
      sexo: req.body.sexo,
      especialidad: req.body.especialidad
    }
  }
    const registros = await alumnosDb.insertar(params);
    console.log("-------------- registros " + registros);

  } catch (error) {
    console.error(error)
    res.status(400).send("sucedio un error: " + error);
  }
  rows = await alumnosDb.mostrarTodos();
  res.render('alumnos', { reg: rows, titulo: "Alumnos"});
});

router.post("/buscar", async(req,res)=>{
  matricula = req.body.matricula;
  nrow = await alumnosDb.buscarMatricula(matricula);
  res.render('alumnos', {alu:nrow, titulo: "Alumnos"});
})

router.use((req, res, next) => {
  res.status(404).render('404', { titulo: "Error Pagina no Encontrada" });
});


  export default { router };

